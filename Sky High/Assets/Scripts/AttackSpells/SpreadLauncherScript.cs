﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpreadLauncherScript : MonoBehaviour
{
    public float fireRate = 3;
    public Transform firepoint;
    public GameObject projectileToFire;
    public float[] spreadAngles = { 15, 5, -5, -15 };

    //public AudioSource shotSFX;

    private float timeBtwShots;
    private float timer;

    // Set this to true to enable the burn damage effect
    public bool fireType;

    // Start is called before the first frame update
    void Start()
    {
        timeBtwShots = 1 / fireRate;
        if (fireType)
        {
            projectileToFire.GetComponent<FireballScript>().fireType = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;

        if (Input.GetButton("Fire1") && timer >= timeBtwShots)
        {
            FireProjectile();
            timer = 0;
        }
    }


    void FireProjectile()
    {
        for (int i = 0; i < spreadAngles.Length; i++)
        {
            Quaternion spreadRot = firepoint.rotation;
            spreadRot *= Quaternion.Euler(0, spreadAngles[i], 0); 
            Instantiate(projectileToFire, firepoint.transform.position, spreadRot);
        }
    }
}
