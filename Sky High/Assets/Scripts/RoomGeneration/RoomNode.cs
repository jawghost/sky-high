﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class RoomNode : MonoBehaviour
{
    public class NodeCost : IComparable
    {
        public Coordinates coordinates;
        //Total cost so far for the node
        public float gCost;
        //Estimated cost from this node to the goal node
        public float hCost;
        //parent node when it comes to finding paths
        public NodeCost parent;


        public NodeCost(Coordinates incomingCoordinates)
        {
            coordinates = incomingCoordinates;
            hCost = 0.0f;
            gCost = 1.0f;
            parent = null;
        }

        //clears the cost back to the original values
        public void Clear()
        {
            hCost = 0.0f;
            gCost = 1.0f;
            parent = null;
        }

        //IComparable Interface method implementation
        public int CompareTo(object obj)
        {
            NodeCost node = (NodeCost)obj;
            if (hCost < node.hCost)
            {
                return -1;
            }
            if (hCost > node.hCost)
            {
                return 1;
            }
            return 0;
        }
    }

    public NodeCost cost;
    public Coordinates coordinates;
    public Dictionary<Direction, WallNode> WallConnector;
    public RoomType roomType;

    public void Initilize(Coordinates coordinatesInput, RoomType roomTypeInput)
    {
        coordinates = coordinatesInput;
        roomType = roomTypeInput;
        WallConnector = new Dictionary<Direction, WallNode>();
        cost = new NodeCost(coordinatesInput);
    }

    //creates a wall at the space specified
    public void GenerateWall(Direction direction, WallNode wallnode)
    {
        if (WallConnector.ContainsKey(direction))
        {
            if (WallConnector[direction].wallType == WallType.WALL)
            {
                Debug.Log("Replacing wall at " + coordinates.ToString() +
                    "\nin the direction:" + direction.ToString() +
                    "\nwith the type of: " + wallnode.wallType.ToString());

                WallNode nodeBeingRemoved = WallConnector[direction];
                WallConnector.Remove(direction);
                Destroy(nodeBeingRemoved);
            }
            else
            {
                Debug.Log("ERROR: wall at " + coordinates.ToString() +
                        "\nin the direction:" + direction.ToString() +
                        "\nwallNode already exists cannot replaced");
                return;
            }
        }

        //add the wall connector to the room
        WallConnector.Add(direction, wallnode);
    }

    //script that generates items for a room
    void GenerateItems()
    {
        Debug.Log("This method does not do anything yet");


        return;
    }

    //script that generates NPCs for the room
    void GenerateNPCs()
    {
        Debug.Log("This method does not do anything yet");


        return;
    }

}
