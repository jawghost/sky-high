﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floor
{

    int roomLength = 20;                                //room length measured in progrid squares
    public int maxX = 5, maxY = 5;                      //max X and y coordinates on a map
    Coordinates origin;

    List<GameObject> allRooms;
    List<GameObject> specialRooms;
    List<GameObject> roomPrefabs;
    List<GameObject> wallPrefabs;

    public void Initilize(int maxHeight, int maxWidth, int numberOfRooms, int numberOfSpecialRooms,
        List<GameObject> roomPrefabList, List<GameObject> wallPrefabList)
    {
        maxX = maxWidth;
        maxY = maxHeight;
        roomPrefabs = roomPrefabList;
        wallPrefabs = wallPrefabList;
        allRooms = new List<GameObject>();
        specialRooms = new List<GameObject>();

        GenerateNewFloorPlan(numberOfRooms + numberOfSpecialRooms);
        GenerateSpecialRooms();
        GenerateFloorPaths();
        //GenerateFloorGameObjects();

        //rooms are currently created on a coordinate grid
        //rooms currently have been assigned properties
        //rooms currently generate gameobjects
        //rooms currently generate paths

        //need to create a script for the linked doors so that the character can move from them
        // **************************** NEXT STEP: CONTINUE WALL GENERATION *******************
    }


    /* 
    *                  Floor Initilization Methods               
    */

    //code that generates a new floor full of normal rooms with coordinates
    void GenerateNewFloorPlan(int numberOfRooms)
    {
        //set the origin room to the middle
        origin = new Coordinates((maxX / 2) + 1, (maxY / 2) + 1);

        //add a number of rooms equal to the passed in variable
        for (int idx = 0; idx < numberOfRooms; idx++)
        {
            GenerateRandomRoom();
        }

        return;
    }

    //Creates special rooms and replaces their normal room counterpart
    void GenerateSpecialRooms()
    {
        GenerateSpecialRoom(RoomType.START);
        GenerateSpecialRoom(RoomType.BOSS);
        GenerateSpecialRoom(RoomType.ITEM);
        GenerateSpecialRoom(RoomType.SHOP);

        if (GetRoomNode(RoomType.BOSS) == GetRoomNode(RoomType.START))
            Debug.Log("ERROR in finding a suitable boss room");
    }

    //links all generated rooms with paths by creating doorways and walls
    void GenerateFloorPaths()
    {
        //create wall nodes for each special room
        foreach (GameObject room in specialRooms)
        {
            GenerateSpecialRoomWallNodes(room);
        }

        //create an AStar path from the boss room to the start room
        List<GameObject> roomPath = CreatePathAStar(
            GetRoomObject(RoomType.BOSS), GetRoomObject(RoomType.START));

        //                                 this is where the issue arises with generating the boss room path

        //create the room nodes for every room on the path
        CreatePathEntrances(roomPath, WallType.DOOR);

        //create paths to the start node from each special room (except the boss)
        foreach (GameObject room in specialRooms)
        {
            //ensure the room we are looking at is not the start or boss room
            if (room.GetComponent<RoomNode>().roomType != RoomType.START
                || room.GetComponent<RoomNode>().roomType != RoomType.BOSS)
            {
                //create an AStar path from the special room to the start room
                roomPath = CreatePathAStar(room, GetRoomObject(RoomType.START));
                CreatePathEntrances(roomPath, WallType.DOOR);
            }
        }

        //go through the normal room list and ensure there is a path going to the closest special room
        foreach (GameObject roomObject in allRooms)
        {
            if (roomObject.GetComponent<RoomNode>().roomType == RoomType.NORMAL)
            {
                //create a path with the closest special room and create a path to it
                roomPath = FindShortestPath(roomObject, specialRooms);
                CreatePathEntrances(roomPath, WallType.DOOR);
            }
        }

        //add doors all around the start room (if they havent already been made)
        CreateAllEntryRoom(GetRoomObject(RoomType.START), WallType.DOOR);

        //go through the normal room list and put walls at any gaps
        foreach (GameObject roomObject in allRooms)
        {
            CreateAllEntryRoom(roomObject, WallType.WALL);
        }
    }

    //create room game objects according to each rooms' attributes
    void GenerateFloorGameObjects()
    {


    }


    /* 
    *                  Room GameObject Creation & Destruction Methods               
    */

    //Instantiate a new normal room in a random location, starting from the center of the map
    GameObject GenerateRandomRoom()
    {
        Coordinates newRoomCoordinates = new Coordinates(-1, -1);

        if (allRooms != null && allRooms.Count > 0)
        {
            while (newRoomCoordinates.x == -1)
            {
                //randomly generate a room index
                int index = GetRandomRoomIndex();
                Direction first, second, third, fourth;
                first = second = third = fourth = 0;
                RandomizeDirections(ref first, ref second, ref third, ref fourth);

                //check to see if there is an un-occuped room on each edge and assign the coordinate if unoccupied
                if (IsCoordinateAvailable(allRooms[index].GetComponent<RoomNode>().coordinates.GetAdjacentCoordinate(first)))
                    newRoomCoordinates = allRooms[index].GetComponent<RoomNode>().coordinates.GetAdjacentCoordinate(first);
                if (IsCoordinateAvailable(allRooms[index].GetComponent<RoomNode>().coordinates.GetAdjacentCoordinate(second)))
                    newRoomCoordinates = allRooms[index].GetComponent<RoomNode>().coordinates.GetAdjacentCoordinate(second);
                if (IsCoordinateAvailable(allRooms[index].GetComponent<RoomNode>().coordinates.GetAdjacentCoordinate(third)))
                    newRoomCoordinates = allRooms[index].GetComponent<RoomNode>().coordinates.GetAdjacentCoordinate(third);
                if (IsCoordinateAvailable(allRooms[index].GetComponent<RoomNode>().coordinates.GetAdjacentCoordinate(fourth)))
                    newRoomCoordinates = allRooms[index].GetComponent<RoomNode>().coordinates.GetAdjacentCoordinate(fourth);
            }
        }
        else
        {
            newRoomCoordinates = origin;
        }

        return InstantiateRoom(newRoomCoordinates, RoomType.NORMAL);
    }

    //add a special room to the floor
    void GenerateSpecialRoom(RoomType roomType)
    {
        int roomIndex = -1;
        switch (roomType)
        {
            case RoomType.START:
                //create the start room at a random location, replace it with a room of the correct type, then add it to the special room list
                ReplaceRoomWithType(allRooms[GetRandomRoomIndex()], RoomType.START);
                break;
            case RoomType.BOSS:
                //create the boss room as far from the start room as possible, replace it with a room of the correct type, then add it to the special room list
                ReplaceRoomWithType(FindFarthestRoom(GetRoomObject(RoomType.START)), RoomType.BOSS);
                break;
            case RoomType.ITEM:
                //get a special room index, replace it with a room of the correct type, then add it to the special room list
                roomIndex = GetNewSpecialRoomIndex();
                if (roomIndex == -1)
                {
                    Debug.Log("Could not find a special room index for special room: " + roomType);
                    break;
                }
                ReplaceRoomWithType(allRooms[roomIndex], RoomType.ITEM);
                break;
            case RoomType.SHOP:
                //get a special room index, replace it with a room of the correct type, then add it to the special room list
                roomIndex = GetNewSpecialRoomIndex();
                if (roomIndex == -1)
                {
                    Debug.Log("Could not find a special room index for special room: " + roomType);
                    break;
                }
                ReplaceRoomWithType(allRooms[roomIndex], RoomType.SHOP);
                break;
            default:
                Debug.Log("ERROR: Unable to add special room - " + roomType);
                break;
        }

    }

    //return a random room prefab of the requested type from the room prefab list
    GameObject GetRoomPrefab(RoomType prefabRoomType)
    {
        int numberOfOptions = 0;
        List<GameObject> roomOptions = new List<GameObject>();

        //check to see if there is a passed in room option list and there is a room available
        if (roomPrefabs != null && roomPrefabs.Count >= 1)
        {
            foreach (GameObject roomObject in roomPrefabs)
            {
                if (roomObject.GetComponent<RoomNode>().roomType == prefabRoomType)
                {
                    roomOptions.Add(roomObject);
                    numberOfOptions++;
                }
            }
        }

        if (numberOfOptions == 1)
        {
            return roomOptions[0];
        }
        else if (numberOfOptions > 1)
        {
            return roomOptions[Random.Range(0, roomOptions.Count - 1)];
        }

        Debug.Log("ERROR: No room prefab avaiable for roomtype - " + prefabRoomType + " - returning 'null'");
        return null;
    }

    //instantiate a unityObject of the specified room prefab at the coodinate location and initilize the room attached script
    GameObject InstantiateRoom(Coordinates roomCoordinates, RoomType objectRoomType)
    {
        GameObject newRoomObject = MonoBehaviour.Instantiate(GetRoomPrefab(objectRoomType),
            new Vector3(roomCoordinates.x * ((roomLength/2) + 1), 0, roomCoordinates.y * ((roomLength/2) + 1)), new Quaternion(0, 0, 0, 0));

        newRoomObject.GetComponent<RoomNode>().Initilize(roomCoordinates, objectRoomType);

        //add the room to the all rooms list
        allRooms.Add(newRoomObject);

        //if it is not a normal room... add it to the special rooms list
        if (objectRoomType != RoomType.NORMAL)
            specialRooms.Add(newRoomObject);

        return newRoomObject;
    }

    //replaces room with a room type specified by the argument, returns a room gameObject
    GameObject ReplaceRoomWithType(GameObject roomBeingDestroyed, RoomType newRoomType)
    {
        //get the coodinates of the room being replaced
        Coordinates roomReplaceCoodinates = roomBeingDestroyed.GetComponent<RoomNode>().coordinates;

        //remove the previous room & destroy the game Object
        DestroyRoom(GetRoomObject(roomReplaceCoodinates));

        //create a new room and initilize it with the last room coordinates and the new room type, then return it
        return InstantiateRoom(roomReplaceCoodinates, newRoomType);
    }

    //Destroy a unityObject of a room and removes the item from all room lists
    void DestroyRoom(GameObject roomBeingDestroyed)
    {
        allRooms.Remove(roomBeingDestroyed);
        specialRooms.Remove(roomBeingDestroyed);
        MonoBehaviour.Destroy(roomBeingDestroyed);
    }


    /* 
    *                  Room Coodinate & Pathfinding Methods                 
    */

    //check to see if a coodinate is avaiable for room placement
    bool IsCoordinateAvailable(Coordinates coordinate)
    {
        //ensure the coordinate is not out of bounds
        if (coordinate.x > maxX || coordinate.x < 1)
            return false;

        if (coordinate.y > maxY || coordinate.y < 1)
            return false;

        //check each room to see if the coordinate matches another
        foreach (GameObject roomObject in allRooms)
        {
            if (coordinate.ToVector2() == roomObject.GetComponent<RoomNode>().coordinates.ToVector2())
                return false;
        }

        return true;
    }

    bool IsCoordinateAvailable(GameObject roomObject)
    {
        //ensure the room coordinate is not out of bounds
        if (roomObject.GetComponent<RoomNode>().coordinates.x > maxX || roomObject.GetComponent<RoomNode>().coordinates.x < 1)
            return false;

        if (roomObject.GetComponent<RoomNode>().coordinates.y > maxY || roomObject.GetComponent<RoomNode>().coordinates.y < 1)
            return false;

        //check each room to see if the coordinate matches the coordinates of the input room object
        foreach (GameObject room in allRooms)
        {
            if (roomObject.GetComponent<RoomNode>().coordinates.ToVector2() == room.GetComponent<RoomNode>().coordinates.ToVector2())
                return false;
        }

        return true;
    }

    //check to see if a coodinate if occupied or if it is out of bounds
    bool IsCoordinateOccupied(Coordinates coordinate)
    {
        //ensure the coordinate is not out of bounds
        if (coordinate.x > maxX || coordinate.x < 1)
            return false;

        if (coordinate.y > maxY || coordinate.y < 1)
            return false;

        //check each room to see if the coordinate matches another
        foreach (GameObject roomObject in allRooms)
        {
            if (coordinate.ToVector2() == roomObject.GetComponent<RoomNode>().coordinates.ToVector2())
                return true;
        }

        return false;
    }

    bool IsCoordinateOccupied(GameObject roomObject)
    {
        //ensure the room coordinate is not out of bounds
        if (roomObject.GetComponent<RoomNode>().coordinates.x > maxX || roomObject.GetComponent<RoomNode>().coordinates.x < 1)
            return false;

        if (roomObject.GetComponent<RoomNode>().coordinates.y > maxY || roomObject.GetComponent<RoomNode>().coordinates.y < 1)
            return false;

        //check each room to see if the coordinate matches the coordinates of the input room object
        foreach (GameObject room in allRooms)
        {
            if (roomObject.GetComponent<RoomNode>().coordinates.ToVector2() == room.GetComponent<RoomNode>().coordinates.ToVector2())
                return true;
        }

        return false;
    }

    //randomly generate four non consecutive directions
    void RandomizeDirections(ref Direction first, ref Direction second, ref Direction third, ref Direction fourth)
    {
        first = (Direction)Random.Range(0, 3);
        second = first;
        third = first;
        fourth = 0;

        while (second == first)
            second = (Direction)Random.Range(0, 3);
        while (third == first || third == second)
            third = (Direction)Random.Range(0, 3);
        while (fourth == first || fourth == second || fourth == third)
            fourth++;
    }

    //create a path of a specific wall connector type based on the passed in roomObject path
    void CreatePathEntrances(List<GameObject> path, WallType wallConnectorType)
    {
        if (path != null && path.Count > 1)
        {
            int index = 0;
            GameObject lastRoom = path[0];
            GameObject nextRoom = path[0];
            foreach (GameObject currentRoom in path)
            {
                if (index == 0)
                {
                    nextRoom = path[index + 1];

                    //ensure bars get built for boss rooms no matter what
                    if (currentRoom.GetComponent<RoomNode>().roomType == RoomType.BOSS
                        || nextRoom.GetComponent<RoomNode>().roomType == RoomType.BOSS)
                    {
                        GenerateWallNode(currentRoom, Coordinates.FaceDirection(currentRoom.GetComponent<RoomNode>().coordinates,
                            nextRoom.GetComponent<RoomNode>().coordinates), WallType.BARS);
                    }
                    else
                    {
                        GenerateWallNode(currentRoom, Coordinates.FaceDirection(currentRoom.GetComponent<RoomNode>().coordinates,
                            nextRoom.GetComponent<RoomNode>().coordinates), wallConnectorType);
                    }

                }
                else if (index > 0 && index < path.Count - 2)
                {
                    nextRoom = path[index + 1];

                    //ensure bars get built for boss rooms no matter what
                    if (lastRoom.GetComponent<RoomNode>().roomType == RoomType.BOSS
                        || currentRoom.GetComponent<RoomNode>().roomType == RoomType.BOSS)
                    {
                        GenerateWallNode(currentRoom, Coordinates.FaceDirection(currentRoom.GetComponent<RoomNode>().coordinates,
                            lastRoom.GetComponent<RoomNode>().coordinates), WallType.BARS);
                    }
                    else
                    {
                        GenerateWallNode(currentRoom, Coordinates.FaceDirection(currentRoom.GetComponent<RoomNode>().coordinates,
                            lastRoom.GetComponent<RoomNode>().coordinates), wallConnectorType);
                    }

                    //ensure bars get built for boss rooms no matter what
                    if (currentRoom.GetComponent<RoomNode>().roomType == RoomType.BOSS
                        || nextRoom.GetComponent<RoomNode>().roomType == RoomType.BOSS)
                    {
                        GenerateWallNode(currentRoom, Coordinates.FaceDirection(currentRoom.GetComponent<RoomNode>().coordinates,
                            nextRoom.GetComponent<RoomNode>().coordinates), WallType.BARS);
                    }
                    else
                    {
                        GenerateWallNode(currentRoom, Coordinates.FaceDirection(currentRoom.GetComponent<RoomNode>().coordinates,
                            nextRoom.GetComponent<RoomNode>().coordinates), wallConnectorType);
                    }
                }
                else if (index == path.Count - 1)
                {
                    //ensure bars get built for boss rooms no matter what
                    if (lastRoom.GetComponent<RoomNode>().roomType == RoomType.BOSS
                        || currentRoom.GetComponent<RoomNode>().roomType == RoomType.BOSS)
                    {
                        GenerateWallNode(currentRoom, Coordinates.FaceDirection(currentRoom.GetComponent<RoomNode>().coordinates,
                            lastRoom.GetComponent<RoomNode>().coordinates), WallType.BARS);
                    }
                    else
                    {
                        GenerateWallNode(currentRoom, Coordinates.FaceDirection(currentRoom.GetComponent<RoomNode>().coordinates,
                            lastRoom.GetComponent<RoomNode>().coordinates), wallConnectorType);
                    }
                }

                lastRoom = currentRoom;
                index++;
            }
        }
    }

    //get a list of neighboring nodes relative to the one passed in
    List<GameObject> GetFringeNodes(GameObject roomObject)
    {
        RoomNode currentRoomNode = roomObject.GetComponent<RoomNode>();
        List<GameObject> fringeNodes = new List<GameObject>();

        if (IsThereARoom(currentRoomNode.coordinates.GetAdjacentCoordinate(Direction.NORTH)))
            fringeNodes.Add(GetRoomObject(currentRoomNode.coordinates.GetAdjacentCoordinate(Direction.NORTH)));

        if (IsThereARoom(currentRoomNode.coordinates.GetAdjacentCoordinate(Direction.EAST)))
            fringeNodes.Add(GetRoomObject(currentRoomNode.coordinates.GetAdjacentCoordinate(Direction.EAST)));

        if (IsThereARoom(currentRoomNode.coordinates.GetAdjacentCoordinate(Direction.SOUTH)))
            fringeNodes.Add(GetRoomObject(currentRoomNode.coordinates.GetAdjacentCoordinate(Direction.SOUTH)));

        if (IsThereARoom(currentRoomNode.coordinates.GetAdjacentCoordinate(Direction.WEST)))
            fringeNodes.Add(GetRoomObject(currentRoomNode.coordinates.GetAdjacentCoordinate(Direction.WEST)));

        return fringeNodes;
    }

    //returns a list of gameobjects in the order of the most cost efficent from the starting room to the ending room
    List<GameObject> CreatePathAStar(GameObject startingRoom, GameObject endingRoom)
    {
        GameObject currentNode = startingRoom;
        GameObject goalNode = endingRoom;
        List<GameObject> fringeNodes = new List<GameObject>();
        PsuedoQueue<RoomNode.NodeCost> exploredNodes = new PsuedoQueue<RoomNode.NodeCost>();
        PsuedoQueue<RoomNode.NodeCost> unExploredNodes = new PsuedoQueue<RoomNode.NodeCost>();

        //add the starting room to and costs to the explored list
        currentNode.GetComponent<RoomNode>().cost.gCost = 0.0f;
        currentNode.GetComponent<RoomNode>().cost.hCost = Coordinates.GetDistance(
            currentNode.GetComponent<RoomNode>().coordinates,
            goalNode.GetComponent<RoomNode>().coordinates);
        unExploredNodes.Push(currentNode.GetComponent<RoomNode>().cost);

        //while we still have nodes to explore
        while (unExploredNodes.Length != 0)
        {
            //set the current node to the first in the unexplored list
            currentNode = GetRoomObject(unExploredNodes.GetFirstObject().coordinates);

            //if the current node is our goal...
            if (currentNode == goalNode)
            {
                //made out path, time to break
                break;
            }

            //clear the fringe and get all new finge nodes
            fringeNodes.Clear();
            fringeNodes = GetFringeNodes(currentNode);

            //Update the costs of each finge node.
            for (int index = 0; index < fringeNodes.Count; index++)
            {
                GameObject fringeNode = fringeNodes[index];

                if (!exploredNodes.Contains(fringeNode.GetComponent<RoomNode>().cost))
                {
                    //Cost from current node to this neighbor node
                    float cost = Coordinates.GetDistance(currentNode.GetComponent<RoomNode>().coordinates,
                        fringeNode.GetComponent<RoomNode>().coordinates);

                    float wallCost = 0;
                    Coordinates currentCoordinates = currentNode.GetComponent<RoomNode>().coordinates;
                    Coordinates fringeCoordinates = fringeNode.GetComponent<RoomNode>().coordinates;
                    Direction directionFacing = Coordinates.GetOppositeDirection(Coordinates.FaceDirection(currentCoordinates, fringeCoordinates));
                    
                    //determine the wall travel cost according to the wall connector facing the current node from the fringe
                    if (!IsThereAWallConnectorAlready(fringeCoordinates, directionFacing))
                        wallCost = 0;
                    else
                    {
                        WallType wallType = fringeNode.GetComponent<RoomNode>().WallConnector[directionFacing].wallType;

                        switch (wallType)
                        {
                            case WallType.DOOR:
                                wallCost = 0;
                                break;
                            case WallType.BARS:
                                wallCost = 1;
                                break;
                            case WallType.WALL:
                                wallCost = 50;
                                break;
                            default:
                                Debug.Log("Error determining the cost of wallType: " 
                                    + wallType.ToString() + " when using AStar Algorithm");
                                break;
                        }
                    }

                    //Total Cost So Far from start to this neighbor node
                    float totalCost = currentNode.GetComponent<RoomNode>().cost.gCost + cost + wallCost;

                    //Estimated cost for neighbor node to the goal
                    float neighborNodeEstCost = Coordinates.GetDistance(fringeNode.GetComponent<RoomNode>().coordinates,
                        goalNode.GetComponent<RoomNode>().coordinates);

                    //Assign neighbor node properties
                    fringeNode.GetComponent<RoomNode>().cost.gCost = totalCost;

                    fringeNode.GetComponent<RoomNode>().cost.parent = currentNode.GetComponent<RoomNode>().cost;

                    fringeNode.GetComponent<RoomNode>().cost.hCost = totalCost + neighborNodeEstCost;

                    //Add the neighbor node to the open list if we haven't already done so.
                    if (!unExploredNodes.Contains(fringeNode.GetComponent<RoomNode>().cost))
                    {
                        unExploredNodes.Push(fringeNode.GetComponent<RoomNode>().cost);
                    }
                }
            }
            exploredNodes.Push(currentNode.GetComponent<RoomNode>().cost);
            unExploredNodes.Remove(currentNode.GetComponent<RoomNode>().cost);
        }

        //build a path from the parent data in the dictionary
        List<GameObject> currentPath = new List<GameObject>();
        currentNode = endingRoom;
        goalNode = startingRoom;

        while (currentNode != null)
        {
            currentPath.Add(currentNode);

            if (currentNode.GetComponent<RoomNode>().cost.parent != null)
                currentNode = GetRoomObject(currentNode.GetComponent<RoomNode>().cost.parent.coordinates);
            else
                currentNode = null;


            if (currentPath.Count > 20)
                break;

        }
        currentPath.Reverse();

        //clear all node costs
        foreach (GameObject roomObject in allRooms)
        {
            roomObject.GetComponent<RoomNode>().cost.Clear();
        }

        return currentPath;
    }

    //finds the shortest path to the current room from the list provided
    List<GameObject> FindShortestPath(GameObject currentRoom, List<GameObject> roomOptions)
    {
        List<GameObject> roomPath;
        int pathLength = CreatePathAStar(currentRoom, roomOptions[0]).Count;
        GameObject closestRoom = roomOptions[0];

        //loop through each special room and determine which one is closest, store that variable
        foreach (GameObject roomOption in roomOptions)
        {
            roomPath = CreatePathAStar(currentRoom, roomOption);
            if (pathLength > roomPath.Count)
            {
                pathLength = roomPath.Count;
                closestRoom = roomOption;
            }
        }

        return CreatePathAStar(currentRoom, closestRoom);
    }

    /* 
    *                  Wall & Doorway Methods            
    */

    //create wall nodes specifically for special rooms, also passes in AStar path
    void GenerateSpecialRoomWallNodes(GameObject specialRoom)
    {
        RoomType type = specialRoom.GetComponent<RoomNode>().roomType;
        switch (type)
        {
            case RoomType.START:
                break;
            case RoomType.BOSS:
                List<GameObject> roomPath;
                roomPath = CreatePathAStar(GetRoomObject(RoomType.BOSS), GetRoomObject(RoomType.START));

                CreateSingleEntryRoom(GetRoomObject(RoomType.BOSS), Coordinates.FaceDirection(
                    roomPath[0].GetComponent<RoomNode>().coordinates,
                    roomPath[1].GetComponent<RoomNode>().coordinates), WallType.BARS);
                break;
            case RoomType.ITEM:
                CreateAllEntryRoom(GetRoomObject(RoomType.ITEM), WallType.BARS);
                break;
            case RoomType.SHOP:
                CreateAllEntryRoom(GetRoomObject(RoomType.SHOP), WallType.DOOR);
                break;
        }
    }

    //return a random wall prefab of the requested type from the wall prefab list
    GameObject GetWallTypePrefab(WallType prefabWallType)
    {
        int numberOfOptions = 0;
        List<GameObject> wallOptions = new List<GameObject>();

        //check to see if there is a passed in wall option list and there is a wall available
        if (wallPrefabs != null && wallPrefabs.Count >= 1)
        {
            foreach (GameObject wallObject in wallPrefabs)
            {
                //EDIT THIS TO WORK WITH NEW WALL OBJECT SCRIPT
                if (wallObject.GetComponent<WallNode>().wallType == prefabWallType)
                {
                    wallOptions.Add(wallObject);
                    numberOfOptions++;
                }
            }
        }

        if (numberOfOptions == 1)
        {
            return wallOptions[0];
        }
        else if (numberOfOptions > 1)
        {
            return wallOptions[Random.Range(0, wallOptions.Count - 1)];
        }

        Debug.Log("ERROR: No wall prefab avaiable for wallType - " + prefabWallType + " - returning 'null'");
        return null;
    }

    //create a room with three walls and a single point of entry
    void CreateSingleEntryRoom(GameObject roomObject, Direction doorDirection, WallType doorType)
    {
        Direction first, second, third, fourth;
        first = second = third = fourth = 0;

        //*** need to find out where there are applicable single entry points first... **

        //create the current room with only one entrance facing the specified direction
        RandomizeDirections(ref first, ref second, ref third, ref fourth);
        GenerateWallNode(roomObject, doorDirection, doorType);
        if (first != doorDirection)
            GenerateWallNode(roomObject, first, WallType.WALL);
        if (second != doorDirection)
            GenerateWallNode(roomObject, second, WallType.WALL);
        if (third != doorDirection)
            GenerateWallNode(roomObject, third, WallType.WALL);
        if (fourth != doorDirection)
            GenerateWallNode(roomObject, fourth, WallType.WALL);
    }

    //create a room with no walls and all points of entry
    void CreateAllEntryRoom(GameObject roomObject, WallType doorType)
    {
        Direction first, second, third, fourth;
        first = second = third = fourth = 0;

        //create all single specified doortype around the current room generated in a random order
        RandomizeDirections(ref first, ref second, ref third, ref fourth);
        GenerateWallNode(roomObject, first, doorType);
        GenerateWallNode(roomObject, second, doorType);
        GenerateWallNode(roomObject, third, doorType);
        GenerateWallNode(roomObject, fourth, doorType);
    }

    //check to see if two coordinates are next to eachtoher
    bool IsCoodinateAdjacent(Coordinates firstCoordinate, Coordinates secondCoordinate)
    {
        if (firstCoordinate.ToVector2() == secondCoordinate.ToVector2())
            return true;
        if (firstCoordinate.GetAdjacentCoordinate(Direction.NORTH).ToVector2() == secondCoordinate.ToVector2())
            return true;
        if (firstCoordinate.GetAdjacentCoordinate(Direction.SOUTH).ToVector2() == secondCoordinate.ToVector2())
            return true;
        if (firstCoordinate.GetAdjacentCoordinate(Direction.EAST).ToVector2() == secondCoordinate.ToVector2())
            return true;
        if (firstCoordinate.GetAdjacentCoordinate(Direction.WEST).ToVector2() == secondCoordinate.ToVector2())
            return true;

        return false;
    }

    //builds a door if the wallspace is not occupied, builds a wall if there is nothing occupying the adjacent space
    void GenerateWallNode(GameObject roomObject, Direction direction, WallType wallType)
    {
        //check to see if there is already a wall connection and return if that is the case
        if (IsThereAWallConnectorAlready(roomObject, direction))
            return;

        //if the room next to this one is not occupied, build a wall instead of a doorway
        if (!IsCoordinateOccupied(GetAdjacentCoordinates(roomObject, direction)))
        {
            InstantiateWallNode(roomObject, direction, WallType.WALL);
        }
        else  //if the room is occupied, create a wall on both sides and link it
            InstantiateWallNode(roomObject, direction, wallType);
    }

    //Creates a unityObject door or wall of the specified type. Builds a wall if there is no room in the specified direction
    void InstantiateWallNode(GameObject roomObject, Direction direction, WallType wallType)
    {
        //mod values for rotating and transforming wall connectors
        int xMod = 0;
        int yMod = 0;
        int rotationMod = 0;

        //change the values of the mods based on the direction of the wall being built
        if (direction == Direction.NORTH)
        {
            yMod = roomLength / 4;
            rotationMod = 180;
        }
        if (direction == Direction.SOUTH)
        {
            yMod = -roomLength / 4;
            rotationMod = 0;
            //Direction.South = no change to rotation
        }
        if (direction == Direction.EAST)
        {
            xMod = roomLength / 4;
            rotationMod = 270;
        }
        if (direction == Direction.WEST)
        {
            xMod = -roomLength / 4;
            rotationMod = 90;
        }

        //if we are building a doorway of some kind... and not a wall
        if (wallType != WallType.WALL)
        {
            //create a new wall object and adjust it to fit in the wall space
            GameObject wallObject = MonoBehaviour.Instantiate(GetWallTypePrefab(wallType), roomObject.transform) as GameObject;
            wallObject.transform.Translate(new Vector3(xMod, 0, yMod));
            wallObject.transform.Rotate(0, rotationMod, 0);
            roomObject.GetComponent<RoomNode>().GenerateWall(direction, wallObject.GetComponent<WallNode>());

            //create the opposite wall object to pair with the original wall
            Direction oppositeDirection = Coordinates.GetOppositeDirection(direction);
            GameObject adjacentRoomObject = GetAdjacentRoom(roomObject, direction);
            //change the values of the mods based on the opposite direction of the wall being built
            if (oppositeDirection == Direction.NORTH)
            {
                yMod = roomLength / 4;
                rotationMod = 180;
            }
            if (oppositeDirection == Direction.SOUTH)
            {
                yMod = -roomLength / 4;
                rotationMod = 0;
                //Direction.South = no change to rotation
            }
            if (oppositeDirection == Direction.EAST)
            {
                xMod = roomLength / 4;
                rotationMod = 270;
            }
            if (oppositeDirection == Direction.WEST)
            {
                xMod = -roomLength / 4;
                rotationMod = 90;
            }
            //create a new wall object and adjust it to fit in the wall space
            GameObject adjacentWallObject = MonoBehaviour.Instantiate(GetWallTypePrefab(wallType), adjacentRoomObject.transform) as GameObject;
            adjacentWallObject.transform.Translate(new Vector3(xMod, 0, yMod));
            adjacentWallObject.transform.Rotate(0, rotationMod, 0);
            adjacentRoomObject.GetComponent<RoomNode>().GenerateWall(oppositeDirection, adjacentWallObject.GetComponent<WallNode>());

            //link the two wall nodes together
            wallObject.GetComponent<WallNode>().WallLink(adjacentWallObject);
        }
        else   //otherwise... if we are building just a simple wall... just build a single wall
        {
            //create a new wall object and adjust it to fit in the wall space
            GameObject wallObject = MonoBehaviour.Instantiate(GetWallTypePrefab(wallType), roomObject.transform) as GameObject;
            wallObject.transform.Translate(new Vector3(xMod, 0, yMod));
            wallObject.transform.Rotate(0, rotationMod, 0);
            roomObject.GetComponent<RoomNode>().GenerateWall(direction, wallObject.GetComponent<WallNode>());
        }
    }

    //check to see if there is a wall connector on a specific corner of a room
    bool IsThereAWallConnectorAlready(Coordinates coordinate, Direction direction)
    {
        RoomNode roomBeingTested = GetRoomNode(coordinate);
        if (roomBeingTested == null)
            return false;
        else
            return roomBeingTested.WallConnector.ContainsKey(direction);
    }

    bool IsThereAWallConnectorAlready(GameObject roomObject, Direction direction)
    {
        RoomNode roomBeingTested = roomObject.GetComponent<RoomNode>();
        if (roomBeingTested == null)
            return false;
        else
            return roomBeingTested.WallConnector.ContainsKey(direction);
    }

    //randomizes what kind of door the player has to deal with based on level difficulty
    WallType RandomizeEntry()
    {
        Debug.Log("Adding a door entry, the doorways have not been randomized yet");
        return WallType.DOOR;
    }

    WallType RandomizeEntry(bool wallsIncluded)
    {
        Debug.Log("Adding a door entry, the doorways and walls have not been randomized yet");
        return WallType.DOOR;
    }


    /* 
     *                  Room Game Object & Room Object Misc Methods                 
    */

    //return the index of a viable special room placemment, returns -1 if none are avaiable
    int GetNewSpecialRoomIndex()
    {
        bool foundRoom = false;
        int roomIndex = GetRandomRoomIndex();

        for (int index = 0; index <= allRooms.Count - 1; index++)
        {
            foreach (GameObject roomObject in specialRooms)
            {
                //if a special room is next to one that we want to place...
                if (IsCoodinateAdjacent(allRooms[roomIndex].GetComponent<RoomNode>().coordinates, roomObject.GetComponent<RoomNode>().coordinates))
                {
                    //set the flag to false and break the loop
                    foundRoom = false;
                    break;
                }
                else
                    foundRoom = true;
            }

            //if we found a room... return the index
            if (foundRoom)
                return roomIndex;

            //if the room index exceeds the number of elements, reset it to 0
            if (roomIndex >= allRooms.Count - 1)
                roomIndex = 0;
            else //add one to the room index
                roomIndex++;
        }

        //check to see if the new coodinate is adjacent to the start and boss special rooms
        for (int index = 0; index <= allRooms.Count - 1; index++)
        {
            foreach (GameObject roomObject in specialRooms)
            {
                //if a start or boss room is next to one that we want to place... 
                //OR the room is not a boss or start room and is in the special room's coordinates already
                if ((roomObject.GetComponent<RoomNode>().roomType == RoomType.BOSS
                    && IsCoodinateAdjacent(allRooms[roomIndex].GetComponent<RoomNode>().coordinates, roomObject.GetComponent<RoomNode>().coordinates))
                    || (roomObject.GetComponent<RoomNode>().roomType != RoomType.BOSS
                    && roomObject.GetComponent<RoomNode>().coordinates.ToVector2() == allRooms[roomIndex].GetComponent<RoomNode>().coordinates.ToVector2()))
                {
                    //set the flag to false and break the loop
                    foundRoom = false;
                    break;
                }
                else
                    foundRoom = true;
            }

            //if we found a room... return the index
            if (foundRoom)
                return roomIndex;

            //if the room index exceeds the number of elements, reset it to 0
            if (roomIndex >= allRooms.Count - 1)
                roomIndex = 0;
            else //add one to the room index
                roomIndex++;
        }

        //if there were no options found, return -1
        return -1;
    }

    //finds the longest path by direct distance in order to determine the room with the most distance
    GameObject FindFarthestRoom(GameObject startingRoomObject)
    {
        float greatestDistance = 0f;
        RoomNode endRoom, startRoom;
        endRoom = startRoom = startingRoomObject.GetComponent<RoomNode>();

        foreach (GameObject roomObject in allRooms)
        {
            if (Vector2.Distance(startRoom.coordinates.ToVector2(), roomObject.GetComponent<RoomNode>().coordinates.ToVector2()) > greatestDistance)
            {
                greatestDistance = Vector2.Distance(startRoom.coordinates.ToVector2(), roomObject.GetComponent<RoomNode>().coordinates.ToVector2());
                endRoom = roomObject.GetComponent<RoomNode>();
            }
        }

        return GetRoomObject(endRoom.coordinates);
    }

    //return a room GameObject based on the input coodinates
    GameObject GetRoomObject(Coordinates inputCoordinates)
    {
        //check each room to see if the coordinate matches another
        foreach (GameObject roomObject in allRooms)
        {
            if (roomObject.GetComponent<RoomNode>().coordinates.ToVector2() == inputCoordinates.ToVector2())
                return roomObject;
        }

        Debug.Log("ERROR: Unable to find a room with coordinates:" + inputCoordinates.ToString());
        return null;
    }

    GameObject GetRoomObject(RoomType roomType)
    {
        //check each room to see if the coordinate matches another
        foreach (GameObject roomObject in allRooms)
        {
            if (roomObject.GetComponent<RoomNode>().roomType == roomType)
                return roomObject;
        }

        Debug.Log("ERROR: Unable to find a room by roomType:" + roomType.ToString());
        return null;
    }

    //get the room next to the one specified though input, returns null if unable to find the room
    GameObject GetAdjacentRoom(GameObject roomObject, Direction direction)
    {
        Coordinates adjacentCoordinates = roomObject.GetComponent<RoomNode>().coordinates.GetAdjacentCoordinate(direction);
        return GetRoomObject(adjacentCoordinates);
    }

    //get the room next to the one specified though input, returns null if unable to find the room
    Coordinates GetAdjacentCoordinates(GameObject roomObject, Direction direction)
    {
        return roomObject.GetComponent<RoomNode>().coordinates.GetAdjacentCoordinate(direction);
    }

    //checks to see if there is a roomObject based on input coodinates
    bool IsThereARoom(Coordinates inputCoordinates)
    {
        //check each room to see if the coordinate matches another
        foreach (GameObject roomObject in allRooms)
        {
            //if the coodinates match one of the rooms... return true
            if (roomObject.GetComponent<RoomNode>().coordinates.ToVector2() == inputCoordinates.ToVector2())
                return true;
        }

        //return false if there is no room object or node at the specified location
        return false;
    }

    //return a roomNode based on the input coodinates
    RoomNode GetRoomNode(Coordinates inputCoordinates)
    {
        //check each room to see if the coordinate matches another
        foreach (GameObject roomObject in allRooms)
        {
            if (roomObject.GetComponent<RoomNode>().coordinates.ToVector2() == inputCoordinates.ToVector2())
                return roomObject.GetComponent<RoomNode>();
        }

        Debug.Log("Unable to find a RoomNode with coordinates:" + inputCoordinates.ToString());
        return null;
    }

    //return a roomNode based on the room type
    RoomNode GetRoomNode(RoomType roomType)
    {
        //check each room to see if the coordinate matches another
        foreach (GameObject roomObject in allRooms)
        {
            if (roomObject.GetComponent<RoomNode>().roomType == roomType)
                return roomObject.GetComponent<RoomNode>();
        }

        Debug.Log("ERROR: Unable to find a RoomNode by roomType:" + roomType.ToString());
        return null;
    }

    //get a random room index
    int GetRandomRoomIndex()
    {
        return Random.Range(0, allRooms.Count - 1);
    }

    //finds the room index of the room being checked
    int GetRoomIndex(RoomNode roomBeingChecked)
    {
        for (int index = 0; index <= allRooms.Count - 1; index++)
        {
            if (allRooms[index].GetComponent<RoomNode>() == roomBeingChecked)
                return index;
        }

        Debug.Log("ERROR: unable to get room index of RoomNode:" 
            + roomBeingChecked.GetComponent<RoomNode>().coordinates.ToString());
        return -1;
    }

    int GetRoomIndex(GameObject roomBeingChecked)
    {
        for (int index = 0; index <= allRooms.Count - 1; index++)
        {
            if (allRooms[index] == roomBeingChecked)
                return index;
        }

        Debug.Log("ERROR: unable to get room index of room GameObject:"
            + roomBeingChecked.GetComponent<RoomNode>().coordinates.ToString());
        return -1;
    }

}
