﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapHandler : MonoBehaviour
{
    [SerializeField] private List<GameObject> roomPrefabs;                      //the room prefabs
    [SerializeField] private List<GameObject> wallPrefabs;                      //the wall prefabs
    Floor currentFloor;

    // Start is called before the first frame update
    void Start()
    {
        Initilize();
    }

    void Initilize()
    {
        //create nessecary objects and map variables
        currentFloor = new Floor();
        currentFloor.Initilize(5,5, Random.Range(5,7) ,2, roomPrefabs, wallPrefabs);
    }


}
