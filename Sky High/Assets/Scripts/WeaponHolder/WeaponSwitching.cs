﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class WeaponSwitching : MonoBehaviour
{
    // To enable weapon switching, this script is to be attatched to an empty object
    // preferably named "WeaponHolder". Spell launchers are then attached to the WeaponHolder. 
    // Order of the weapons starts with 0, with the top-most object being 0
    // And lastly, be sure the WeaponHolder is attached to a player.

    // If debug is set to true, you will be able to swap weapons with the number keys.
    public bool debugMode = false; 
    public int selectedWeapon = 0;
    private int previousWepon;

    // Start is called before the first frame update
    void Start()
    {
        SelectWeapon();
    }

    // Update is called once per frame
    void Update()
    {
        if (debugMode)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                selectedWeapon = 0;
            }
            else if (Input.GetKeyDown(KeyCode.Alpha2) && transform.childCount >= 2)
            {
                selectedWeapon = 1;
            }
            else if (Input.GetKeyDown(KeyCode.Alpha3) && transform.childCount >= 3)
            {
                selectedWeapon = 2;
            }
            else if (Input.GetKeyDown(KeyCode.Alpha4) && transform.childCount >= 4)
            {
                selectedWeapon = 3;
            }
            else if (Input.GetKeyDown(KeyCode.Alpha5) && transform.childCount >= 5)
            {
                selectedWeapon = 4;
            }
            else if (Input.GetKeyDown(KeyCode.Alpha6) && transform.childCount >= 6)
            {
                selectedWeapon = 5;
            }
            else if (Input.GetKeyDown(KeyCode.Alpha7) && transform.childCount >= 7)
            {
                selectedWeapon = 6;
            }
        }
        

        if (previousWepon != selectedWeapon)
        {
            SelectWeapon();
            previousWepon = selectedWeapon;
        }
    }

    
    void SelectWeapon()
    {
        int i = 0;

        foreach(Transform weapon in transform)
        {
            if(i == selectedWeapon)
            {
                weapon.gameObject.SetActive(true);
            }
            else if (i != selectedWeapon)
            {
                weapon.gameObject.SetActive(false);
            }
            i++;
        }
    }
}
