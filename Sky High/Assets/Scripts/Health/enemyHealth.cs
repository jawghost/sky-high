﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyHealth: MonoBehaviour
{
    public float Health = 100;
    public float FlashTime = .2f;

    private float currentealth;

    public float fireTickDamage = 10;
    public float secondsBtwTicks = 1;
    public int maxTicks = 3;

    private MeshRenderer enemyMesh;
    private Color originalColor;
    private Color flashColor = Color.red;

    void Awake()
    {
        enemyMesh = GetComponent<MeshRenderer>();
        originalColor = enemyMesh.material.color;
        currentealth = Health;
    }

    void Update()
    {
        if (Health <= 0)
        {
            Die();
        }
    }

    public void takeDamage(float damage)
    {
        Health -= damage;

        StartCoroutine(DamageFlash());
    }

    public void takeFireDamage(float damage)
    {
        takeDamage(damage);
        StartCoroutine(Burn());
        
    }

    public void takeLightningDamage(float damage)
    {

    }

    public void takeIceDamge(float damage)
    {

    }

    void burn()
    {
        takeDamage(fireTickDamage);
        //StartCoroutine(TimeBtwTicks());
        takeDamage(fireTickDamage);
        //StartCoroutine(TimeBtwTicks());
        takeDamage(fireTickDamage);
    }
    void freeze()
    {

    }
    void shock()
    {

    }

    IEnumerator DamageFlash()
    {
        enemyMesh.material.color = flashColor;
        yield return new WaitForSeconds(FlashTime);
        enemyMesh.material.color = originalColor;
       
    }

    IEnumerator Burn()
    {
        int i = 0;
        while(i < maxTicks)
        {
            takeDamage(fireTickDamage);
            yield return new WaitForSeconds(secondsBtwTicks);
            i++;
        }
        
    }

    public void Die()
    {
        Destroy(gameObject);
    }
}